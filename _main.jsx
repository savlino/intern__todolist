/*
Приложение отображает список задач. Возможности:
* Добавить задачу. Инпут, в который можно ввести название и кнопка, добавляющая задачу.
* Пометить задачу выполненной / снять отметку. Кнопка «edit» внутри элемента списка, либо щелчок по элементу списка.
* Удалить задачу из списка. Кнопка «delete» внутри элемента списка. */

import React, {Component} from "react";

class App extends Component {
  render() {
    return <TodoList />;
  }
}

class TodoList extends Component {
  state = {
    list: {}
  };

  handleCreate = (text) => {
    this.setState(state => {
      const id = generateUid();
      return {
        list: {
          ...state.list,
          [id]: {id, text, completed: false}
        }
      }
    });
  };

  handleUpdate = todo => {
    this.setState(state => ({
      list: {
        ...state.list,
        [todo.id]: todo
      }
    }));
  };

  handleDelete = id => {
    this.setState(({list}) => {
      const newList = {...list};
      delete newList[id];
      return {
        list: newList
      };
    });
  };

  render() {
    return (
      <div>
        <InputTask onTodoAdd={this.handleCreate} />
        {Object.values(this.state.list).map((todo) => (
          <Todo
            todo={todo}
            handleDelete={this.handleDelete}
            handleUpdate={this.handleUpdate}
          />
        ))}
      </div>
    );
  }
}

class Todo extends Component {
  state = {isEditing: false};

  onDelete = () => {
    const {handleDelete, todo} = this.props;
    handleDelete(todo.id);
  };

  onToggle = () => {
    const {handleUpdate, todo} = this.props;
    handleUpdate({...todo, completed: !todo.completed});
  };

  onUpdateText = () => {
    const {handleUpdate, todo} = this.props;
    handleUpdate({...todo, text: this.input.value});
    this.onToggleEdit();
  };

  onToggleEdit = () => {
    this.setState(state => ({
      isEditing: !state.isEditing,
    }));
  };

  onInputRef = (node) => {
    this.input = node;
  };

  render() {
    const {todo} = this.props;
    return (
      <div key={todo.id}>
        <input
          type="checkbox"
          checked={todo.completed}
          onChange={this.onToggle}
        />
        {this.state.isEditing ? (
          <React.Fragment>
            <input defaultValue={todo.text} ref={this.onInputRef}/>
            <button onClick={this.onUpdateText}>OK</button>
            <button onClick={this.onToggleEdit}>notOK</button>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <span
              style={{
                textDecoration: todo.completed ? 'line-through' : 'none'
              }}
            >
              {todo.text}
            </span>
            <button onClick={this.onDelete}>delete</button>
            <button onClick={this.onToggleEdit}>edit</button>
          </React.Fragment>
        )}
      </div>
    );
  }
}

class InputTask extends Component {
  state = {
    text: ''
  };

  handleChange = (e) => {
    this.setState({
      text: e.target.value,
    })
  };

  submit = (event) => {
    event.preventDefault();
    this.props.onTodoAdd(this.state.text);
    this.setState({text: ''});
  };

  render() {
    return(
      <div>
        <form onSubmit={this.submit}>
          <legend>type your task, now:</legend>
          <input
            type="text"
            name="new-task"
            value={this.state.text}
            onChange={this.handleChange}
          />
          <button>create</button>
        </form>
      </div>
    )
  }
}

function generateUid () {
  return Math.round(Math.random() * 1000000);
}

export default App;